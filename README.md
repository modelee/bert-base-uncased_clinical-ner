A Named Entity Recognition model for clinical entities (`problem`, `treatment`, `test`)

The model has been trained on the [i2b2 (now n2c2) dataset](https://n2c2.dbmi.hms.harvard.edu) for the 2010 - Relations task. Please visit the n2c2 site to request access to the dataset.